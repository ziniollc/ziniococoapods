Pod::Spec.new do |s|

  s.name         = "ZinioSDK"
  s.version      = "1.9.0"
  s.summary      = "ZinioSDK is a propietary SDK for reading magazines in mobile platforms."

  s.description  = <<-DESC
                    The SDK provides access to the following use cases:
                    * Download and delete an issue.
                    * Open the reader for an issue.
                    * Manage Bookmarks.
                   DESC

  s.homepage     = "http://www.zinio.com"
  s.license      = { :type => "Copyrigth", :file => "LICENSE" }
  s.author       = "Zinio LLC"

  s.platform     = :ios, "9.0"
  s.ios.deployment_target = "9.0"

  s.source       = { :git => "git@bitbucket.org:ziniollc/ios-reader-demo.git", :tag => "v.#{s.version}" }

  s.vendored_frameworks = "ZinioSDK.framework"

  s.requires_arc = true

  s.dependency "Result", "~> 3.0"
  s.dependency "Unbox", "~> 2.5"
  s.dependency "Zip", "1.0"
  s.dependency "EVReflection", "~> 5.0"  
  s.dependency "RealmSwift", "~> 3.0.2"  
  s.dependency "ReachabilitySwift", "~> 3.0"
  s.dependency "SKPhotoBrowser", "5.0.0"
  s.dependency "lottie-ios"
  s.dependency "ZinioAnalytics", "1.3"
  s.dependency "AlamofireImage", "~> 3.3"

end