Pod::Spec.new do |s|

  s.name         = "ZinioSDK"
  s.version      = "1.2.1"
  s.summary      = "ZinioSDK is a propietary SDK for reading magazines in mobile platforms."

  s.description  = <<-DESC
                    The SDK provides access to the following use cases:
                    * Download and delete an issue.
                    * Open the reader for an issue.
                    * Manage Bookmarks.
                   DESC

  s.homepage     = "http://www.zinio.com"
  s.license      = { :type => "Copyrigth", :file => "LICENSE" }
  s.author       = "Zinio LLC"

  s.platform     = :ios, "9.0"
  s.ios.deployment_target = "9.0"

  s.source       = { :git => "git@bitbucket.org:ziniollc/ios-reader-demo.git", :tag => "v.#{s.version}" }

  s.vendored_frameworks = "ZinioSDK.framework"

  s.requires_arc = true

  s.dependency "Zip", "0.8.0"
  s.dependency "EVReflection", "~> 3.3"  
  s.dependency "RealmSwift", "2.8.3"
  s.dependency "ReachabilitySwift", "~> 3.0"
  s.dependency "Result", "~> 3.0"
  s.dependency "Unbox", "~> 2.5"
  s.dependency "SKPhotoBrowser", "4.1.1"
  s.dependency "lottie-ios"
  s.dependency "ZinioAnalytics"

end
