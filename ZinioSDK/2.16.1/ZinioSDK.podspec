Pod::Spec.new do |s|

    s.name         = "ZinioSDK"
    #START version
    s.version      = "2.16.1"
    #END version
    s.summary      = "ZinioSDK is a propietary SDK for reading magazines in mobile platforms."

    s.description  = <<-DESC
                            The SDK provides access to the following use cases:
                            * Download and delete an issue.
                            * Open the reader for an issue.
                            * Manage Bookmarks.
                           DESC

    s.homepage     = "http://www.zinio.com"
    s.license      = { :type => "Copyrigth", :file => "LICENSE" }
    s.author       = "Zinio LLC"

    s.platform     = :ios, "9.3"
    s.ios.deployment_target = "9.3"

    s.source       = { :git => "git@bitbucket.org:ziniollc/ios-reader-demo.git", :tag => "v.#{s.version}" }

    s.vendored_frameworks = "ZinioSDK.framework"
    s.swift_version = "4.2"

    s.requires_arc = true

    #START dependencies
    s.dependency 'Zip', '~> 1.0'
    s.dependency 'RealmSwift', '3.18.0'
    s.dependency 'SKPhotoBrowser', '6.0.0'    
    s.dependency 'AlamofireImage', '~> 3.5'
    s.dependency 'Moya/RxSwift', '~> 12.0'
    s.dependency 'Result', '~> 4.0'
    s.dependency 'Unbox', '~> 3.0'
    s.dependency 'ReachabilitySwift', '~> 4.3.0'
    s.dependency 'KeychainAccess', '~> 3.1'
    s.dependency 'Swinject', '~> 2.5'
    s.dependency 'SwiftSoup', '~> 1.7'
    s.dependency 'SwiftLint', '~> 0.29'
    s.dependency 'RxAtomic', '4.4.0'
    s.dependency 'SnapKit', '4.2.0'
    #END dependencies

end
