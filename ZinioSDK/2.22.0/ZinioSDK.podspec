Pod::Spec.new do |s|
  s.name = 'ZinioSDK'
  s.version = '2.22.0'
  s.summary = 'ZinioSDK is a propietary SDK for reading magazines in mobile platforms.'
  
  s.description  = <<-DESC
  The SDK provides access to the following use cases:
  * Download and delete an issue.
  * Open the reader for an issue.
  * Manage Bookmarks.
  DESC
  
  s.homepage = 'http://www.zinio.com'
  s.license = { type: 'Copyrigth', file: 'LICENSE' }
  s.author = 'Zinio LLC'
  
  s.platform = :ios, '11.0'
  s.ios.deployment_target = '11.0'
  
  s.source = { git: 'git@bitbucket.org:ziniollc/ios-reader-demo.git', tag: "v.#{s.version}" }
  
  s.vendored_frameworks = 'ZinioSDK.framework'
  s.swift_version = '5.0'
  
  s.requires_arc = true
  
  # dependencies
  s.dependency 'Zip', '1.1.0'
  s.dependency 'RealmSwift', '3.18.0'
  s.dependency 'SKPhotoBrowser', '6.1.0'
  s.dependency 'Moya/RxSwift', '~>14.0'
  s.dependency 'RxRelay', '~>5.1.0'
  s.dependency 'ReachabilitySwift', '4.3.0'
  s.dependency 'KeychainAccess', '4.1.0'
  s.dependency 'Swinject', '2.5.0'
  s.dependency 'SwiftSoup', '1.7.4'
  s.dependency 'SwiftLint', '~> 0.36'
  s.dependency 'SnapKit', '5.0.1'  
  s.dependency 'Kingfisher', '5.13.0'
end
