Pod::Spec.new do |s|

  s.name         = "ZinioAnalytics"
  s.version      = "1.3.1"
  s.summary      = "ZinioAnalytics is a propietary SDK for tracking analytics of Zinio LLC products"

  s.description  = <<-DESC
                    ZinioAnalytics is a propietary SDK for tracking analytics of Zinio LLC products.
                    It let's you add different trackers that get notified of the events that happen inside
                    the ZinioSDK.
                   DESC

  s.homepage     = "http://www.zinio.com"
  s.license      = { :type => "Copyrigth", :file => "LICENSE" }
  s.author       = "Zinio LLC"

  s.platform     = :ios, "9.0"
  s.ios.deployment_target = "9.0"

  s.source       = { :git => "git@bitbucket.org:ziniollc/ios-analytics-sdk.git", :tag => "v.#{s.version}" }

  s.source_files       = 'ZinioAnalytics/*.swift', 'ZinioAnalytics/*.h'
  s.requires_arc = true

end