Pod::Spec.new do |s|
  s.name = 'ReaderSDK'
  s.version = '2.54.0'
  s.summary = 'ReaderSDK is a propietary SDK for reading magazines in mobile platforms.'
  
  s.description  = <<-DESC
  The SDK provides access to the following use cases:
  * Download and delete issues.
  * Open the reader for an issue.
  * Open the reader for an article.
  * Manage Bookmarks.
  DESC
  
  s.homepage = 'http://www.zinio.com'
  s.license = { type: 'Copyright', file: 'LICENSE.md' }
  s.author = 'Zinio LLC'
  
  s.platform = :ios, '13.0'
  s.ios.deployment_target = '13.0'

  s.source = { git: 'git@bitbucket.org:ziniollc/ios-reader-demo.git', tag: "#{s.version}" }
  
  s.vendored_frameworks = 'ReaderSDK.xcframework'
  s.swift_version = '5.7.1'
  
  s.requires_arc = true
  
  # dependencies
  s.dependency 'ZIPFoundation', '0.9.16'
  s.dependency 'RealmSwift', '10.47.0'
  s.dependency 'RxRelay', '6.6.0'
  s.dependency 'KeychainAccess', '4.2.2'
  s.dependency 'SnapKit', '5.7.1'
  s.dependency 'Kingfisher', '7.10.2'
end
