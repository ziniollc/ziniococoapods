Pod::Spec.new do |s|
  s.name = 'ReaderSDK'
  s.version = '2.37.0'
  s.summary = 'ReaderSDK is a propietary SDK for reading magazines in mobile platforms.'
  
  s.description  = <<-DESC
  The SDK provides access to the following use cases:
  * Download and delete an issue.
  * Open the reader for an issue.
  * Manage Bookmarks.
  DESC
  
  s.homepage = 'http://www.zinio.com'
  s.license = { type: 'Copyright', file: 'LICENSE' }
  s.author = 'Zinio LLC'
  
  s.platform = :ios, '12.0'
  s.ios.deployment_target = '12.0'
  
  s.source = { git: 'git@bitbucket.org:ziniollc/ios-reader-demo.git', tag: "v.#{s.version}" }
  
  s.vendored_frameworks = 'ReaderSDK.framework'
  s.swift_version = '5.4'
  
  s.requires_arc = true
  
  # dependencies
  s.dependency 'Zip', '1.1.0'
  s.dependency 'RealmSwift', '3.18.0'
  s.dependency 'SKPhotoBrowser', '6.1.0'
  s.dependency 'RxRelay', '~>5.1.0'
  s.dependency 'KeychainAccess', '4.1.0'
  s.dependency 'SnapKit', '5.0.1'  
  s.dependency 'Kingfisher', '5.14.1'
end
