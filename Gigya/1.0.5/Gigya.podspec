Pod::Spec.new do |s|
  s.name             = 'Gigya'
  s.version          = '1.0.5'
  s.description      = 'See a description on the homepage'
  s.summary          = 'See a summary on the homepage'

  s.homepage         = 'https://developers.gigya.com/display/GD/Swift+SDK'
  s.author           = 'Gigya, Inc.'
  s.source           = { :git => 'git@bitbucket.org:ziniollc/gigya-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'
  s.swift_version = '5.1.2'

  s.vendored_frameworks = 'Gigya.framework'
end
